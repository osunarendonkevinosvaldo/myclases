/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agregacionbomba;

/**
 *
 * @author hp
 */
public class Gasolina {
    private int id ;
    private String marca;
    private int tipo;
    private float precio;

    public Gasolina() {
        this.id = 0;
        this.marca = "";
        this.tipo = 0;
        this.precio = 0.0f;
    }

    public Gasolina(int id, String marca, int tipo, float precio) {
        this.id = id;
        this.marca = marca;
        this.tipo = tipo;
        this.precio = precio;
    }
    
    public Gasolina (Gasolina gasolina){
        this.id = gasolina.id;
        this.marca =gasolina.marca;
        this.tipo = gasolina.tipo;
        this.precio = gasolina.precio;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }
    
    public String mostrarInformacion(){
        String informacion = "";
        informacion = " ID " + this.id + "Tipo" + this.tipo + " marca " + this.marca  + " Precio " + this.precio;
        return informacion;
    }
    
    
    }

