/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Herencia;

/**
 *
 * @author hp
 */
public abstract class Empleado {
 protected int numEmpleado;
 protected String nomEmpleado;
 protected String puesto;
 protected String depto;

    public Empleado() {
        this.numEmpleado = 0;
        this.nomEmpleado = "";
        this.puesto = "";
        this.depto = "";
    }
 
    public Empleado(int numEmpleado, String nomEmpleado, String puesto, String depto) {
        this.numEmpleado = numEmpleado;
        this.nomEmpleado = nomEmpleado;
        this.puesto = puesto;
        this.depto = depto;
    }
    

    public int getNumEmpleado() {
        return numEmpleado;
    }

    public void setNumEmpleado(int numEmpleado) {
        this.numEmpleado = numEmpleado;
    }

    public String getNomEmpleado() {
        return nomEmpleado;
    }

    public void setNomEmpleado(String nomEmpleado) {
        this.nomEmpleado = nomEmpleado;
    }

    public String getPuesto() {
        return puesto;
    }

    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }

    public String getDepto() {
        return depto;
    }

    public void setDepto(String depto) {
        this.depto = depto;
    }
 
    public abstract float calcularPago();
 
 
 
}
