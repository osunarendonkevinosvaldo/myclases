/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Herencia;

/**
 *
 * @author hp
 */
public class EmpleadoBase extends Empleado implements Impuesto {

    private float pagoDiario;
    private float diasTra;

    public EmpleadoBase() {
        this.pagoDiario = 0.0f;
        this.diasTra = 0.0f;
    }

    public EmpleadoBase(float pagoDiario, float diasTra, int numEmpleado, String nomEmpleado, String puesto, String depto) {
        super(numEmpleado, nomEmpleado, puesto, depto);
        this.pagoDiario = pagoDiario;
        this.diasTra = diasTra;
    }

    public float getPagoDiario() {
        return pagoDiario;
    }

    public void setPagoDiario(float pagoDiario) {
        this.pagoDiario = pagoDiario;
    }

    public float getDiasTra() {
        return diasTra;
    }

    public void setDiasTra(float diasTra) {
        this.diasTra = diasTra;
    }
    
    
    
    
    @Override
    public float calcularPago() {
        return this.diasTra * this.pagoDiario;    
    }

    @Override
    public float calcularImpuesto() {
        float impuestos = 0.0f;
        if(this.calcularPago()>5000)
            impuestos = this.calcularPago()* .16f;
        
        
        return impuestos;
    }
    
}
